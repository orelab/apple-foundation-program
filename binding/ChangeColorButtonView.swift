//
//  LievreView.swift
//  testIos
//
//  Created by Aurélien CHIROT on 24/10/2023.
//

import SwiftUI

struct ChangeColorButtonView: View {
    @Binding var color: Color
    @State var picto: String = "folder"
    @State var label: String = "Hello"
    @State var btnColor: Color = .red
    
    var body: some View {
        Button(action: { color = btnColor }) {
            Label(label, systemImage: picto)
        }
            .buttonStyle(.borderedProminent)
            .tint(btnColor)
            .foregroundStyle(.white)
    }
}


struct ChangeColorButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ChangeColorButtonView(color: .constant(.red), picto: "folder", label: "Hi all !", btnColor: .gray)
    }
}
