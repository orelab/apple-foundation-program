//
//  LievreTortueView.swift
//  testIos
//
//  Created by Aurélien CHIROT on 24/10/2023.
//

import SwiftUI




struct BindingView: View {
    
    @State var bgColor:Color = .white
    
    var body: some View {
        ZStack {
            bgColor
            
            HStack {
                ChangeColorButtonView(color: $bgColor, picto: "tortoise.fill", label: "", btnColor: .red)
                ChangeColorButtonView(color: $bgColor, picto: "music.note", label: "", btnColor: .blue)
                ChangeColorButtonView(color: $bgColor, picto: "hare.fill", label: "", btnColor: .red)
                ChangeColorButtonView(color: $bgColor, picto: "music.house.fill", label: "", btnColor: .blue)
            
                Text("coucou")
                    .font(.custom("Vegan Style Personal Use", size: 18))
            }
        }.ignoresSafeArea()
    }
}


struct BindingView_preview: PreviewProvider {
    static var previews: some View {
        BindingView()
    }
}



