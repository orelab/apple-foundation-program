//
//  SwiftUIView.swift
//  AFP
//
//  Created by Aurélien CHIROT on 02/07/2024.
//

import SwiftUI

struct SwiftUIView: View {
    
    @State var phototheque = false
    @State var appareilPhoto = false
    @State var chargement = false

    
    
    var body: some View {
        

                Menu {
                    Button("Depuis la photothèque", action: {phototheque = true})
                    Button("Depuis l'appareil photo", action: {appareilPhoto = true})
                    Button("Charger un fichier", action: {chargement = true})
                } label : {
                    Label("Charger une photo", systemImage: "photo.artframe")
                }
                .sheet(isPresented: $phototheque, content: {
                    Text("phototheque !")
                    // ...
                })
                .sheet(isPresented: $appareilPhoto, content: {
                    Text("appareil photo !")
                    // ...
                })
                .sheet(isPresented: $chargement, content: {
                    Text("chargement d'un fichier !")
                    // ...
                })

        }

    

        
}

#Preview {
    SwiftUIView()
}
