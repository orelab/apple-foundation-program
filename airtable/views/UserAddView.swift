//
//  UserAddView.swift
//  AFP
//
//  Created by Aurélien CHIROT on 13/02/2024.
//

import SwiftUI

struct UserAddView: View {
    
    @State var firstname:String = ""
    
    @State var lastname:String = ""
    
    @State var email:String = ""
    
    @State var phone:String = ""
    
    @State var message:String = ""
    @State var messageColor:Color = .green

    func save() async {
        let user = UserType(
            id: nil,
            firstname: firstname,
            lastname: lastname,
            email: email,
            phone: phone
        )
        if await WebService().addUser(user) {
            message = "saved!"
            messageColor = .green
            firstname = ""
            lastname = ""
            email = ""
            phone = ""
        } else {
            message = "cannot deal with Airtable"
            messageColor = .red
        }
        
    }

    var body: some View {
        VStack {
            
            if message != "" {
                ZStack {
                    messageColor
                    Text(message)
                        .foregroundStyle(.white)
                }
                .frame(height: 50)
            }
            

            NavigationView {
                List {
                    TextField("Firstname", text: $firstname)
                    TextField("Lastname", text: $lastname)
                    TextField("Email", text: $email)
                    TextField("Phone", text: $phone)
                    Button {
                        Task {
                            await save()
                        }
                    } label: {
                        Label("save", systemImage: "square.and.arrow.down")
                    }
                }
                .navigationTitle("New user")
                .navigationBarTitleDisplayMode(.inline)
                

            }

        }
        
    }
}

#Preview {
    UserAddView()
}
