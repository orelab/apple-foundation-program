//
//  UserListView.swift
//  AFP
//
//  Created by Aurélien CHIROT on 12/02/2024.
//

import SwiftUI

struct AddressListView: View {

    @State var error:String = ""

    @State var addresses:[AddressRecord] = []
    
    func fetchData() async {
        guard let result:AddressRecords = await WebService().getAddresses() else {
            error = "cannot deal with Airtable"
            return
        }
        
        error = ""
        print(result.records)
        addresses = result.records
    }
    
    func AddressText(_ address: AddressType) -> some View {
        var road:String = address.road1 ?? ""
        road += "\r"
        road += address.road2 ?? ""
        road += address.road2 == nil ? "" : "\r"
        road += address.postcode ?? ""
        road += " "
        road += address.city ?? ""
        
        return Text(road)
    }
    
    var body: some View {
        
        VStack {
            
            if error != "" {
                ZStack {
                    Color.red
                    Text(error)
                        .foregroundStyle(.white)
                }
                .frame(height: 50)
            }
            
            NavigationView {
                
                List(addresses, id: \.id){ address in
                    AddressText(address.fields)
                }
                .navigationTitle(addresses.count>0 ? "Airtable addresses" : "empty table")
                .navigationBarTitleDisplayMode(.inline)
                
            }
            .task{
                await fetchData()
            }
        }
    }
}

#Preview {
    AddressListView()
}
