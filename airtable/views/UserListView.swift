//
//  UserListView.swift
//  AFP
//
//  Created by Aurélien CHIROT on 12/02/2024.
//

import SwiftUI

struct UserListView: View {

    @State var error:String = ""

    @State var users:[UserRecord] = []
    
    func fetchData() async {
        guard let result:UserRecords = await WebService().getUsers() else {
            error = "cannot deal with Airtable"
            return
        }
        
        error = ""
        print(result.records)
        users = result.records
    }
    
    func UserText(_ user: UserType) -> some View {
        let firstname = user.firstname ?? "John"
        let lastname = user.lastname ?? "Doe"
        
        return Text("\(firstname) \(lastname)")
    }
    
    var body: some View {
        
        if error != "" {
            ZStack {
                Color.red
                Text(error)
                    .foregroundStyle(.white)
            }
            .frame(height: 50)
        }
        
        NavigationView {
            
            List(users, id: \.id){ user in
                UserText(user.fields)
            }
            .navigationTitle("Airtable users")
            .navigationBarTitleDisplayMode(.inline)
            
        }
        .task{
            await fetchData()
        }
    }
}

#Preview {
    UserListView()
}
