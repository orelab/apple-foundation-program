//
//  Constants.swift
//
//  https://medium.com/@jpmtech/create-your-own-one-question-survey-using-swiftui-and-airtable-be7702d71e3f
//
//


import Foundation




class Config {
    
    var baseId:String = ""

    var personalAccessToken:String = ""

    var usersTableId:String = ""

    var addressesTableId:String = ""
    
    init(){
        if let airtable_baseId = Bundle.main.infoDictionary?["AIRTABLE_BASEID"] as? String {
            self.baseId = airtable_baseId
            print("L'ID AIRTABLE EST \(self.baseId)")

        }
        if let airtable_personalAccessToken = Bundle.main.infoDictionary?["AIRTABLE_PERSONALACCESSTOKEN"] as? String {
            self.personalAccessToken = airtable_personalAccessToken
        }
        if let airtable_usersTableId = Bundle.main.infoDictionary?["AIRTABLE_USERTABLE"] as? String {
            self.usersTableId = airtable_usersTableId
        }
        if let airtable_addressesTableId = Bundle.main.infoDictionary?["AIRTABLE_ADDRESSTABLE"] as? String {
            self.addressesTableId = airtable_addressesTableId
        }
        
    }
}
