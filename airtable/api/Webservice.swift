//
//  WebService.swift
//
//  https://medium.com/@jpmtech/create-your-own-one-question-survey-using-swiftui-and-airtable-be7702d71e3f
//
//


import Foundation

enum httpMethod: String {
    case connect = "CONNECT"
    case delete = "DELETE"
    case get = "GET"
    case head = "HEAD"
    case options = "OPTIONS"
    case patch = "PATCH"
    case post = "POST"
    case put = "PUT"
    case trace = "TRACE"
}

enum NetworkError: Error {
    case badUrl
    case invalidRequest
    case badResponse
    case badStatus
    case failedToDecodeResponse
}

class WebService {
    
    let url = "https://api.airtable.com/v0"
    
    let config:Config = Config()
    
    
    func getUsers<T: Codable>() async -> T? {
        return await self.get(fromURL: "\(url)/\(config.baseId)/\(config.usersTableId)")
    }
    
    func getAddresses<T: Codable>() async -> T? {
        return await self.get(fromURL: "\(url)/\(config.baseId)/\(config.addressesTableId)")
    }

    private func get<T: Codable>(fromURL: String) async -> T? {
        do {
            guard let url = URL(string: fromURL) else { throw NetworkError.badUrl }
            var request = URLRequest(url: url)
            request.setValue("Bearer \(config.personalAccessToken)", forHTTPHeaderField: "Authorization")
            let (data, response) = try await URLSession.shared.data(for: request)
            print(data)
//            print(response)
            // ----------------
            guard let response = response as? HTTPURLResponse else { throw NetworkError.badResponse }
            guard response.statusCode >= 200 && response.statusCode < 300 else { throw NetworkError.badStatus }
            let decodedResponse = try JSONDecoder().decode(T.self, from: data)
            
            return decodedResponse
        } catch NetworkError.badUrl {
            print("There was an error creating the URL")
        } catch NetworkError.badResponse {
            print("Did not get a valid response")
        } catch NetworkError.badStatus {
            print("Did not get a 2xx status code from the response")
        } catch NetworkError.failedToDecodeResponse {
            print("Failed to decode response into the given type")
        } catch {
            print("An error occured downloading the data")
        }
        return nil
    }
    
    
    func addUser(_ user:UserType) async -> Bool {
        let userRecord = UserRecord(
            id: nil,
            createdTime: nil,
            fields: user
        )
        return await self.add(record:userRecord, toURL: "\(url)/\(config.baseId)/\(config.usersTableId)")
    }
    
    func addAddress(_ address:AddressRecord) async -> Bool {
        return await self.add(record:address, toURL: "\(url)/\(config.baseId)/\(config.addressesTableId)")
    }

    private func add(record: Record, toURL: String) async -> Bool {
        do {
            guard let url = URL(string: toURL) else { throw NetworkError.badUrl }
            var request = URLRequest(url: url)
            request.httpMethod = httpMethod.post.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(config.personalAccessToken)", forHTTPHeaderField: "Authorization")

            guard let jsonBody = try? JSONEncoder().encode(record) else {return false}
                        
            let (data, response) = try await URLSession.shared.upload(for: request, from: jsonBody)
            
            // print("\nData", String(data: data, encoding: .utf8))
            print("\n\nData", data)
            print("\n\nResponse", response)
            guard let response = response as? HTTPURLResponse else { throw NetworkError.badResponse }
            guard response.statusCode >= 200 && response.statusCode < 300 else {
                print("Status Code", response.statusCode)
                throw NetworkError.badStatus
            }
            
            // we could use this to display the record update that was successfull.
            // but this implementation doesn't use the next line
//            guard let decodedResponse = try? JSONDecoder().decode(UserRecords.self, from: data) else { throw NetworkError.failedToDecodeResponse }
            
            print("Successfully updated the record")
            return true
            
        } catch NetworkError.badUrl {
            print("There was an error creating the URL")
        } catch NetworkError.badResponse {
            print("Did not get a valid response")
        } catch NetworkError.badStatus {
            print("Did not get a 2xx status code from the response")
        } catch NetworkError.failedToDecodeResponse {
            print("Failed to decode response into the given type")
        } catch {
            print("An error occured downloading the data")
        }
        
        return false
    }
}
