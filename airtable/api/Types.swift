//
//  Types.swift
//  AFP
//
//  Created by Aurélien CHIROT on 12/02/2024.
//

import Foundation


struct UserType: Codable {
    let id: Int?
    let firstname: String?
    let lastname: String?
    let email: String?
    let phone: String?
}


struct AddressType: Codable {
    let id: Int?
    let road1: String?
    let road2: String?
    let postcode: String?
    let city: String?
    let owner: UserType?
}


// https://designcode.io/swiftui-advanced-multiple-type-variables

protocol Record: Codable {
    var id: String? { get }
    var createdTime: String? { get }
}

struct UserRecord: Record {
    var id: String?
    var createdTime: String?
    var fields: UserType
}

struct AddressRecord: Record {
    var id: String?
    var createdTime: String?
    var fields: AddressType
}


protocol Records: Codable {
}

struct UserRecords: Records {
    let records: [UserRecord]
}

struct AddressRecords: Records {
    let records: [AddressRecord]
}
