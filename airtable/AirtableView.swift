//
//  AirtableView.swift
//  AFP
//
//  Created by Aurélien CHIROT on 12/02/2024.
//
//
//  Bearly inspired from :
//  https://medium.com/@jpmtech/create-your-own-one-question-survey-using-swiftui-and-airtable-be7702d71e3f
//
//  See that :
//  curl "https://api.airtable.com/v0/appmDF7Y9KoXo6nn8/tbl2RkCiaptSZy2rB" -H "Authorization: Bearer patH4j7YoWanm0NvL.05d1bf307a73ebfa98fc5655022a50e1c4f8e717059864ba5b1e874e2bcd45f2"
//

import SwiftUI





struct AirtableView: View {
        
    var body: some View {
        TabView{
            UserListView()
                .tag(0)
                .tabItem {
                    Label("users", systemImage: "person")
                }
            UserAddView()
                .tag(1)
                .tabItem {
                    Label("add user", systemImage: "person.badge.plus")
                }
            AddressListView()
                .tag(2)
                .tabItem {
                    Label("addresses", systemImage: "tablecells.fill")
                }

        }
    
    }
}

#Preview {
    AirtableView()
}


