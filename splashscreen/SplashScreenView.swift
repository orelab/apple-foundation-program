//
//  SplashScreenView.swift
//  AFP
//
//  Created by Aurélien CHIROT on 01/02/2024.
//
//  Bearly inspired from https://medium.com/@liyicky/how-to-make-a-splash-screen-in-swift-ui-83b02984a6ab
//

import SwiftUI

struct SplashScreenView<Content:  View>: View {
        
    @State var showSplash:Bool = true
    
    var content: Content

    @State var logoSize:Double = 35

    var body: some View {
        
        if !showSplash {
            
            content
            
        } else {
            ZStack {
                Color.pink.opacity(0.4)
                
                LogoSimplon()
                
                Button(action: {
                    withAnimation {
                        self.showSplash = false
                    }
                }, label: {
                    VStack {
                        Spacer()
                        Spacer()
                        
                        Image(systemName: "cursorarrow.click.2")
                            .resizable()
                            .frame(width: logoSize, height: logoSize)
                            .foregroundColor(.white)
                            .padding()
                            .onAppear {
                                let baseAnimation = Animation.easeInOut(duration: 1)
                                let repeated = baseAnimation.repeatForever(autoreverses: true)
                                withAnimation(repeated) {
                                    logoSize = 25
                                } completion: {
                                    withAnimation {
                                        logoSize = 35
                                    }
                                }
                            }
                        
                        Spacer()
                    }
                })
                
            }
            .onAppear {
                DispatchQueue.main.asyncAfter(deadline: .now() + 8.0) {
                    withAnimation {
                        self.showSplash = false
                    }
                }
            }
            .ignoresSafeArea()
        }
    }
}



#Preview {
    SplashScreenView(content: MenuView())
}




struct LogoSimplon: View {
    var body: some View {
        ZStack {
            Circle()
                .trim(from: 0.0, to: 1.0)
                .stroke(
                    Color.pink,
                    style: StrokeStyle(
                        lineWidth: 20,
                        lineCap: .round
                    )
                )
                .frame(width: 150)
            
            VStack {
                Rectangle()
                    .fill(.pink)
                    .frame(width: 25, height: 25)
                
                Spacer()
                
                Rectangle()
                    .fill(.pink)
                    .frame(width: 25, height: 25)
            }
            .frame(height: 70)
        }
    }
}
