//
//  AnimationView.swift
//  AFP
//
//  Created by Aurélien CHIROT on 17/03/2024.
//

import SwiftUI


struct AnimationView: View {
        
    @State var size = 1.0
    @State var alternance = true
    @State var angle = 0.0
    @State var rotation = 0.0
    @State var blur = 0.0
    
    let timer = Timer.publish(every: 1.0, on: .main, in: .common).autoconnect()
    
    var body: some View {
        
        VStack(spacing:50) {

            
            ZStack {
                Circle()
                    .fill(.cyan)
                    .frame(width:105)
 
                Circle()
                    .fill(.white)
                    .frame(width:100)
               
                GeometryReader { proxy in
                    let base_x = proxy.size.width/2
                    let base_y = proxy.size.height/2

                    Path { path in
                        path.move(to: CGPoint(x:base_x-4, y:base_y))
                        path.addLine(to:CGPoint(x:base_x+4, y:base_y))
                        path.addLine(to:CGPoint(x:base_x, y:base_y-50))
                    }
                    .fill(.pink)
                        .rotationEffect(.degrees(angle))
                        .animation(.bouncy(duration:0.3,extraBounce:0.5), value:angle)
                        .onReceive(timer) { input in
                            angle += 6
                        }
                }
                    .frame(height: 150)
            }

            
            HStack {
                
                Button("-30"){
                    withAnimation(.bouncy(duration:0.1,extraBounce:0.6)) {
                        angle -= 30
                    }
                }
                Button("+180"){
                    withAnimation(.default) {
                        angle += 180
                    }
                }
                
            }
            
            
            HStack(spacing:50) {

                Button("coucou"){
                    angle += 10
                }
                .rotationEffect(.degrees(angle))
              //.animation(.default, value:angle)
                
                
                Button {
                    size += alternance ? 2 : -2
                    alternance = !alternance
                    angle += 25
                } label: {
                    PillView(color:.blue, title:"blue pill")
                        .scaleEffect(size)
                        .rotationEffect(.degrees(angle))
                        .blur(radius: blur)
                      //.animation(.linear(duration: 3), value:size)
                }
            }
            
            
            Slider(value: $blur, in:0...20)

            
            Button {
                rotation += 25
            } label:{
                PillView(color: .red, title:"red pill")
                    .scaleEffect(2)
                    .rotationEffect(.degrees(rotation))
                    .animation(.bouncy(duration:0.4,extraBounce:0.5), value:rotation)
            }
            
            
            Slider(value: $rotation, in:0...360)
            
            
            Button("Tap Me") {
                withAnimation(.bouncy(duration:0.1,extraBounce:0.5)) {
                    rotation += 35
                }
            }
            .scaleEffect(3)
            .rotation3DEffect(.degrees(rotation), axis: (x: 1, y: 0, z: 0))

        }
    }
}



#Preview {
    AnimationView()
}



struct PillView: View {
    var color:Color = .blue
    var title = "blue pill"
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/)
                .fill(color)
                .frame(width: 80, height:30)
            Text(title)
                .foregroundStyle(.white)
        }
    }
}
