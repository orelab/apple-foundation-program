//
//  SolarSystemView.swift
//  AFP
//
//  Created by Aurélien CHIROT on 23/04/2024.
//

import SwiftUI


struct Planet: Identifiable {
    var id = UUID()
    var name: String
    var distance: Int // km
    var speed: Int // num days for one rotation
    var reverse: Bool // is sun revolution reversed ?
    var diameter: Int // km
    var color: Color
}


let planets:[Planet] = [
    Planet(name:"Sun", distance: 0, speed: 0, reverse: false, diameter: 1_392_700 / 100, color: .yellow),
    Planet(name:"Mercure", distance: 58_000_000, speed: 88, reverse: false, diameter: 4_878, color: .gray),
    Planet(name:"Vénus", distance: 108_000_000, speed: 225, reverse: true, diameter: 12_102, color: .white),
    Planet(name:"Terre", distance: 150_000_000, speed: 365, reverse: false, diameter: 12_750, color: .blue),
    Planet(name:"Mars", distance: 230_000_000, speed: 686, reverse: false, diameter: 6_792, color: .red),
    Planet(name:"Jupiter", distance: 778_000_000, speed: 4330, reverse: false, diameter: 143_000, color: .brown),
    Planet(name:"Saturne", distance: 1_430_000_000, speed: 10752, reverse: false, diameter: 116_000, color: .yellow),
    Planet(name:"Uranus", distance: 2_800_000_000, speed: 30660, reverse: true, diameter: 51_000, color: .teal),
    Planet(name:"Neptune", distance: 4_500_000_000, speed: 60140, reverse: false, diameter: 49_500, color: .blue),
]


struct SolarSystemView: View {
    
    @State var speed:Double = 10 // 0~100
    @State var zoom:Double = 60 // 0~100
    
    var body: some View {
        ZStack {
            Color.black
            
            VStack {
                
                
                GeometryReader { geometry in
                    ZStack {
                        //Color.black
                        ForEach(planets){ p in
                            PlanetView(planet:p, zoom:$zoom, speed:$speed)
                                .frame(
                                    maxWidth: geometry.size.width,
                                    maxHeight: geometry.size.height - 150
                                )
                        }
                    }
                }
                .ignoresSafeArea()
                
                
                ZStack {
                    VStack {
//                        Text("speed")
//                            .foregroundColor(Color.white)
//                        Slider(value: $speed, in:0...100)
                        
                        Text("zoom")
                            .foregroundColor(Color.white)
                        Slider(value: $zoom, in:10...99)
                    }
                }
                .frame(height:150)
                
                
                
            }
        } .ignoresSafeArea(edges:.top)
    }
}



#Preview {
    SolarSystemView()
}




struct PlanetView: View {
    
    let planet:Planet
    @Binding var zoom:Double
    @Binding var speed:Double
    
    var size:Double {
        let s = 100/(100-zoom)
        //let s = Double(1_000_000 + planet.diameter) / 1_000_000 * ( 100/(100-zoom) )
        return s < 8 ? 8 : s
    }
    
    @State private var rotationAngle = 0.0

    
    var body: some View {
        ZStack {
            //Text("\(zoom)")
            
            Circle()
                .stroke(.red,lineWidth:0.3)
                .shadow(color: Color.red.opacity(1.0), radius: 10)
                .frame(width:Double(planet.distance)*0.000001/(100-zoom)*2, height:Double(planet.distance)*0.000001/(100-zoom)*2)


            Circle()
                .frame(width:CGFloat(Double(planet.diameter)*0.005/(100-zoom)), height:CGFloat(Double(planet.diameter)*0.005/(100-zoom)))
                //.frame(width: size, height: size)
                .foregroundColor(planet.color)
                .offset(
                    x: CGFloat(Double(planet.distance)*0.000001/(100-zoom))
                )
                .rotationEffect(.degrees(rotationAngle), anchor: .center)
                .onAppear {
                    withAnimation(Animation.linear(duration: Double(planet.speed)/100/speed).repeatForever(autoreverses: false)) {
                        rotationAngle += planet.reverse ? -360 : 360
                    }
                }
        }
    }
}
