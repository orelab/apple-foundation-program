//
//  ContentView.swift
//  testIos
//
//  Created by Aurélien CHIROT on 23/10/2023.
//

import SwiftUI




struct ColorPickerView: View {
    @State private var showAlert = false
    @State private var bgColor:Color = .white
    
    @State private var red:Double = 1
    @State private var green:Double = 1
    @State private var blue:Double = 1
    var bgColorIndexed: Color {
        return Color.init(red: red, green: green, blue: blue)
    }
    
    var body: some View {
        
        VStack(spacing:0) {
            ZStack {
                bgColor.ignoresSafeArea()
                
                VStack {
                    HStack {
                        Button("Red") { bgColor = .red }
                            .buttonStyle(.borderedProminent)
                            .tint( bgColor == .red ? .white : .red)
                            .foregroundStyle( bgColor == .red ? .red : .white)
                        
                        Button("Green") { bgColor = .green }
                            .buttonStyle(.borderedProminent)
                            .tint( bgColor == .green ? .white : .green)
                            .foregroundStyle( bgColor == .green ? .green : .white)
                        
                        Button("Blue") { bgColor = .blue }
                            .buttonStyle(.borderedProminent)
                            .tint( bgColor == .blue ? .white : .blue)
                            .foregroundStyle( bgColor == .blue ? .blue : .white)
                    }
                }
            }
            
            
            
            ZStack {
                bgColorIndexed.ignoresSafeArea()
                
                HStack {
                    VStack{
                        Slider(
                            value: $red,
                            in: 0...1
                        )
                        Text("\(red, specifier: "%.2f")")
                    }
                    VStack {
                        Slider(
                            value: $green,
                            in: 0...1
                        )
                        Text("\(green, specifier: "%.2f")")
                    }
                    VStack {
                        Slider(
                            value: $blue,
                            in: 0...1
                        )
                        Text("\(blue, specifier: "%.2f")")
                    }
//                    Text("\(bgColorIndexed.description)")
                }
            }

            
            
        }
    }
}

struct ColorPickerView_Previews: PreviewProvider {
    static var previews: some View {
        ColorPickerView()
    }
}
