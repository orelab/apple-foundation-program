//
//  App.swift
//  AFP
//
//  Created by Aurélien CHIROT on 24/10/2023.
//

import SwiftUI

@main
struct Application: App {
    var body: some Scene {
        WindowGroup {
            SplashScreenView(content: MenuView())
        }
    }
}



struct MenuView: View {
    
    @State var showSplash:Bool = true
    
    @State var tabSelection:Int = 0
    
    var body: some View {
                
        TabView(selection: $tabSelection) {
                ColorPickerView()
                    .tag(0)
                    .tabItem {
                        Label("ColorPicker", systemImage: "paintpalette")
                    }
                ProgressBarView()
                    .tag(1)
                    .tabItem {
                        Label("ProgressBar", systemImage: "goforward")
                    }
                TemperatureView()
                    .tag(2)
                    .tabItem {
                        Label("Temperature", systemImage: "thermometer.medium")
                    }
                BindingView()
                    .tag(3)
                    .tabItem {
                        Label("Binding", systemImage: "folder.fill")
                    }
                MotionView()
                    .tag(4)
                    .tabItem {
                        Label("Motion", systemImage: "figure.walk.motion")
                    }
                SubMenuView()
                    .tag(5)
                    .tabItem {
                        Label("SubMenu", systemImage: "figure.walk.motion")
                    }
                AddressBookView()
                    .tag(6)
                    .tabItem {
                        Label("Table", systemImage: "figure.walk.motion")
                    }
                LogoSimplon()
                    .tag(7)
                    .tabItem {
                        Label("SuperLogo", systemImage: "apple.logo")
                    }
                AirtableView()
                    .tag(8)
                    .tabItem {
                        Label("Airtable", systemImage: "table.badge.more")
                    }
                SolarSystemView()
                    .tag(9)
                    .tabItem {
                        Label("SolarSystem", systemImage: "sun.max.fill")
                    }
            }
        .animation(.easeOut(duration: 2.0), value: tabSelection)
        //    .accentColor(.red)
        
    }
}



struct SubMenuView: View {
    var body: some View {
        HStack {
            Text("Start here =>")
            Menu("actions") {
                Button("Duplicate", action: {})
                Button("Rename", action: {})
                Button("Delete…", action: {})
                Menu("Copy") {
                    Button("Copy", action: {})
                    Button("Copy Formatted", action: {})
                    Button("Copy Library Path", action: {})
                }
            }
        }
    }
}



struct App_Previews: PreviewProvider {
    static var previews: some View {
        SplashScreenView(content: MenuView())
    }
}
