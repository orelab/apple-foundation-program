//
//  PharmaView.swift
//  AFP
//
//  Created by Aurélien CHIROT on 05/02/2024.
//

import SwiftUI


struct Question: Identifiable {
    var id = UUID()
    var question: String
    var answers: [Answer]
}

struct Answer: Identifiable {
    var id = UUID()
    var answer: String
    var choosen: Bool = false
}

var questionnaire: [Question] = [
    Question(question:"pourquoi ?", answers: [
        Answer(answer:"Tais-toi"),
        Answer(answer:"Parce que"),
        Answer(answer:"Hier")
    ]),
    Question(question:"Comment ?", answers: [
        Answer(answer:"Avec les pieds"),
        Answer(answer:"Sur les mains"),
        Answer(answer:"sans regarder")
    ]),
    Question(question:"Où ?", answers: [
        Answer(answer:"Au mont Saint-Michel"),
        Answer(answer:"Dans ton bol de soupe")
    ]),
]





struct PharmaView: View {
    var body: some View {
        NavigationStack {
            QuestionView()
        }
    }
}


struct QuestionView: View {
    var id: Int = 0
    
    var question: Question { questionnaire[id] }
    
    var body: some View {
        Text(question.question)
        HStack {
            ForEach(question.answers) { answer in

                Button(action: {
                    if let answer_id = questionnaire[id].answers.firstIndex(where: { $0.id == answer.id }){
                        questionnaire[id].answers[answer_id].choosen = true
                        print(questionnaire[id].answers[answer_id])
                        
                    }
                }){
                    
                    NavigationLink {
                        if( questionnaire.count > id+1 ){
                            QuestionView(id: id+1)
                        } else {
                            Text("Merci !")
                            AnswersView()
                        }
                    } label: {
                        Text(answer.answer)
                    }
                    
                }
            }
        }
    }
}


struct AnswersView: View {
    var body: some View {
        VStack {
            ForEach(questionnaire){ question in
                ForEach(question.answers){ answer in
                    Text("\(answer.answer) : \(answer.choosen ? "oui" : "non")")
                }
            }
        }
    }
}






#Preview {
    PharmaView()
}
