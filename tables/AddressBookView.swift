//
//  ArrayView.swift
//  AFP
//
//  Created by Aurélien CHIROT on 30/10/2023.
//

import SwiftUI



struct Person: Identifiable {
    let id = UUID()
    let name: String
    let surname: String
    let email: String
    let birthday: Date
    var age: Int? {
        let cal = Calendar.current
        return cal.dateComponents([.year], from: birthday, to: Date.now).year
        //birthday.timeIntervalSinceReferenceDate
    }
}



struct AddressBookView: View {

    @Environment(\.horizontalSizeClass) var sizeCategory

    @State private var addressBook = [
        Person(name:"Orel", surname:"Môrice",email:"at@home dot net",birthday: Date.now.addingTimeInterval(-86400)),
        Person(name:"Michel", surname:"Môrice",email:"at@home",birthday: Date.now.addingTimeInterval(-86400)),
        Person(name:"Stan", surname:"Môrice",email:"at@home",birthday: Date.now.addingTimeInterval(-86400)),
        Person(name:"Simone", surname:"Môrice",email:"at@home",birthday: Date.now.addingTimeInterval(-86400)),
        Person(name:"Jess", surname:"Môrice",email:"at@home",birthday: Date.now.addingTimeInterval(-86400)),
        Person(name:"Dul", surname:"Môrice",email:"at@home",birthday: Date.now.addingTimeInterval(-86400))
    ]
    
    @State private var selectedPeople = Set<Person.ID>()
    
    @State private var sortOrder = [KeyPathComparator(\Person.name)]


    var body: some View {

        VStack {
            
            Grid {
                ForEach(addressBook){ person in
                    GridRow {
                        Text(person.name)
                        Text(person.surname)
                        Text(person.email)
                        Text( DateFormatter().string(from: person.birthday) )
                        //Text("\(person.age)")
                    }
                }
            }
            
            Table(addressBook, selection: $selectedPeople) {
                TableColumn("Name", value: \.name)
                TableColumn("Surname", value: \.surname)
                TableColumn("E-Mail", value: \.email)
            }
//            .onChange(of: sortOrder) {
//                addressBook.sort(using: $0)
//            }
            
            Text("\(selectedPeople.count) people selected")
            
            ForEach(addressBook) { person in
                Text("Hello \(person.name) !") .frame(maxWidth: .infinity, alignment: .leading)
            }

        }
        
    }
}


struct AddressBookView_Previews: PreviewProvider {
    static var previews: some View {
        AddressBookView()
    }
}
