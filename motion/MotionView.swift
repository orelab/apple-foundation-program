//
//  MotionView.swift
//  AFP
//
//  Created by Aurélien CHIROT on 24/10/2023.
//


// https://www.youtube.com/watch?v=ZIs3Gco-lrw


import CoreMotion
import SwiftUI



class MotionManager: ObservableObject{
    private let motionManager = CMMotionManager()
    @Published var x = 0.0
    @Published var y = 0.0
    @Published var z = 0.0

    init() {
        motionManager.deviceMotionUpdateInterval = 1/30      // 1/30e seconds refresh rate
        
        motionManager.startDeviceMotionUpdates(to: .main){[weak self] data, error in
            guard let motion = data?.attitude else { return }
            self?.x = motion.roll
            self?.y = motion.pitch
            self?.z = motion.yaw

        }
    }
}



struct MotionView: View {
    @StateObject private var motion = MotionManager()

    var body: some View {

        Image(systemName:"pencil.circle.fill")
            .foregroundStyle(
                .blue.gradient.shadow(
                    .inner(color:.black, radius:10, x: motion.x * 20, y: motion.y * 20)
                )
            )
            .font(.system(size:250).bold())
            .rotation3DEffect(.degrees(motion.x * 57), axis: (x:0, y:-1, z:0))
            .rotation3DEffect(.degrees(motion.y * 57), axis: (x:1, y:0, z:0))
            .rotation3DEffect(.degrees(motion.z * 57), axis: (x:0, y:0, z:1))
    }
}




struct MotionView_Previews: PreviewProvider {
    static var previews: some View {
        MotionView()
    }
}
