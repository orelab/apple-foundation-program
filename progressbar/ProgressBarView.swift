//
//  ProgressBarView.swift
//  AFP
//
//  Created by Aurélien CHIROT on 23/01/2024.
//

import SwiftUI

struct ProgressBarView: View {
    

    @State private var loading = true // true = appearing ; false = disappear
    @State private var from = 0.0
    @State private var to = 1.0
    private var speed = 0.01
    

    let timer = Timer.publish(every: 0.01, on: .main, in: .common).autoconnect()
    
    
    var body: some View {
        
        ZStack {
            
            Circle()
                .trim(from: from, to: to)
                .stroke(
                    Color.pink,
                    style: StrokeStyle(
                        lineWidth: 20,
                        lineCap: .round
                    )
                )
                .rotationEffect(.degrees(-90))
                .onReceive(timer) { input in
                    
                    if loading && from < 1.0 { // load
                        from += speed
                    }
                    
                    if loading && from >= 1 { // end loading
                        loading = false
                        from = 0.0
                        to = 0.0
                    }
                    
                    if !loading && to<=1.0 { // unload
                        to += speed
                    }
                    
                    if !loading && to >= 1.0 { // end unloading
                        loading = true
                        from = 0.0
                        to = 1.0
                    }
                }
                .frame(width: 150)
            
            
                
            // TWO FIXED POINTS
            
            VStack {
                Rectangle()
                    .fill(.pink)
                    .frame(width: 25, height: 25)
                
                Spacer()
                
                Rectangle()
                    .fill(.pink)
                    .frame(width: 25, height: 25)
            }
            .frame(height: 70)

        }

    }
}

#Preview {
    ProgressBarView()
}

