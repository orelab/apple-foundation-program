# Orel's sandbox for SwiftUI


# Installation 

- Create 2 Airtable arrays :
  * users ( firstname, lastname, email, phone)
  * addresses ( road1, road2, postcode, city)
  * Add a 0-n relation between the tables (add the column "link to another entry")
- retrieve the app ID & the table IDs (from the url)
- in the developer area, generate a personal access token

- git clone git@gitlab.com:orelab/apple-foundation-program.git
- cd apple-foundation-program
- mv Config-sample.xccconfig Config.xcconfig
- edit the file with Airtable's tokens


