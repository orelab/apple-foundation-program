//
//  TemperatureView.swift
//  testIos
//
//  Created by Aurélien CHIROT on 24/10/2023.
//

import SwiftUI

struct DegreeView: View {
    var name: String = "Celsius"
    var value: Double = 10
    var color: Color = .blue
    
    var body: some View {
        ZStack {
            color
            VStack {
                Text(name).foregroundStyle(.white)
                Text("\(value, specifier: "%.2f")").foregroundStyle(.white)
            }
        }
        .ignoresSafeArea()
    }
}

struct DegreeView_Previews: PreviewProvider {
    static var previews: some View {
        DegreeView(name: "Celsius", value: 16, color: .green)
    }
}
