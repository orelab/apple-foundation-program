//
//  CelsiusView.swift
//  testIos
//
//  Created by Aurélien CHIROT on 24/10/2023.
//

import SwiftUI


struct TemperatureView: View {
    @State var degree: Double = 22
    var farenheit: Double { return (degree*9/5) + 32 }
    var kelvin: Double { return degree + 273.15 }
    
    var body: some View {
        
        ZStack(alignment:.bottom) {
            HStack(spacing:0) {
                DegreeView(name:"Celsius", value:degree, color: .orange)
                DegreeView(name:"Farenheit", value:farenheit, color: .blue)
                DegreeView(name:"Kelvin", value:kelvin, color: .purple)
            }
            Slider(value:$degree, in:0...100)
        }
    }
}


struct TemperatureView_Previews: PreviewProvider {
    static var previews: some View {
        TemperatureView()
    }
}
